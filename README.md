This repository holds solutions for miscellaneous problems from HackerRank using Java 8 (at the moment).

Mainly I do this to practice Java 8.