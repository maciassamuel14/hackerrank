import java.util.Collections;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class Solution {

    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
			int n = scanner.nextInt();
			int k = scanner.nextInt();
			SortedSet<Integer> set = Collections.synchronizedSortedSet(new TreeSet<Integer>());
			for (int i = 0; i < n; i++) {
				set.add(scanner.nextInt());
			}
			System.out.println(
					set.stream().mapToInt(i -> i - k).sorted().reduce(0, (a, b) -> (set.contains(b) ? ++a : a)));
		}
    }
}