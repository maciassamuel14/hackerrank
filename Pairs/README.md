[Pairs](https://www.hackerrank.com/challenges/pairs)
=====

Given ***N*** integers, count the number of pairs of integers whose difference is ***K***.

### Input Format ###

The first line contains ***N*** and ***K***.
The second line contains ***N*** numbers of the set. All the ***N*** numbers are unique.

### Constraints ###

* 2 ≤ ***N*** ≤ 10^5
* 0 < ***K*** < 10^9
* Each integer will be greater than *0* and at least *K* smaller than *2^31-1*.

### Output Format ###

An integer that tells the number of pairs of integers whose difference is ***K***.

# TestCase 0 #

### Sample Input ###

```
5 2
1 5 3 4 2
```

### Sample Output ###

```
3
```

# TestCase 1 #

### Sample Input ###

```
10 1
363374326 364147530 61825163 1073065718 1281246024 1399469912 428047635 491595254 879792181 1069262793
```

### Sample Output ###

```
0
```